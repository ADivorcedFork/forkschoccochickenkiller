package org.rspeer.store;

public class Stats {

    private static int profit = 0;

    public static int getProfit() {
        return profit;
    }

    public static void addProfit(int profit) {
        Stats.profit += profit;
    }

}
