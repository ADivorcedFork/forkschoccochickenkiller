package org.rspeer.store;

import org.rspeer.runetek.api.commons.math.Random;

public class Config {

    private static boolean isSetup;

    private static boolean stopping;

    public static int getLoopReturn() {
        return Random.nextInt(300, 600);
    }

    public static boolean isSetup() {
        return isSetup;
    }

    public static void setIsSetup(boolean isSetup) {
        Config.isSetup = isSetup;
    }

    public static boolean isStopping() {
        return stopping;
    }

    public static void setStopping(boolean stopping) {
        Config.stopping = stopping;
    }
}
