package org.rspeer.store;

import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.ui.Log;

public class Store {

    private static StopWatch elapsedTime;
    private static String task;

    public static void startElapsedTime() {
        elapsedTime = StopWatch.start();
    }

    public static String getElapsedTimeString() {
        return elapsedTime == null ? null : elapsedTime.toElapsedString();
    }

    public static String getTask() {
        return task;
    }

    public static void setTask(String task) {
        Store.task = task;
        Log.info("Task: " + task);
    }
}
