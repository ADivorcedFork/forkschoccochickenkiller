package org.rspeer.activity.quest;

import org.rspeer.runetek.api.movement.position.Area;

public enum QuestNpc {

    EASTER_BUNNY("Easter Bunny", Area.rectangular(2971, 3403, 2984, 3395), "Yes"),
    MARTIN("Martin the Master Gardener", Area.rectangular(3068, 3266, 3087, 3249), "Talk about Easter", "Special spices"),
    AGGIE("Aggie", Area.rectangular(3082, 3262, 3089, 3255), "Ask about dyeing"),
    COOK("Cook", Area.rectangular(3205, 3217, 3212, 3211), "Talk about Easter", "Yes", "Easter Bunny", "Easter Egg", "Basket", "Chocolate"),
    MERCHANT("Travelling merchant", Area.rectangular(3287, 3145, 3319, 3135), "Ask about Dried cocoa"),
    SHOP_ASSISTANT("Shop assistant", Area.rectangular(3312, 3186, 3318, 3178), "Talk about Easter"),
    PARTY_PETE("Party Pete", Area.rectangular(3046, 3376, 3055, 3370), "The Easter Bunny");

    private final String name;
    private final Area area;
    private final String[] chatOptions;

    QuestNpc(final String name, final Area area, final String... chatOptions) {
        this.name = name;
        this.area = area;
        this.chatOptions = chatOptions;
    }

    public String getName() {
        return name;
    }

    public Area getArea() {
        return area;
    }

    public String[] getChatOptions() {
        return chatOptions;
    }
}
