package org.rspeer.activity.quest;

import org.rspeer.runetek.api.Varps;

public enum Quest {

    EASTER_EVENT("EasterEvent", 2223, 22318391);

    private final String name;
    private final int varp;
    private final int completedVal;

    Quest(final String name, final int varp, final int completedVal) {
        this.name = name;
        this.varp = varp;
        this.completedVal = completedVal;
    }

    public int getVarp() {
        return varp;
    }

    public boolean isComplete() {
        return Varps.get(varp) == completedVal;
    }

    @Override
    public String toString() {
        return name;
    }
}
