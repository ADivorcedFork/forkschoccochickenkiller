package org.rspeer.activity.quest;

import org.rspeer.api.DialogCompleter;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.api.component.tab.Spell;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.*;
import org.rspeer.ui.Log;
import org.rspeer.store.Config;
import org.rspeer.store.Store;

import java.util.function.Predicate;

public class EasterEvent {

    // Irregular area & position definitions
    private static final Area PUB_AREA = Area.rectangular(3225, 3243, 3237, 3225);
    private static final Area GATE_AREA = Area.rectangular(3246, 3238, 3267, 3219);
    private static final Position SAFE_POSITION = new Position(3253, 3226, 0);
    private static final Position CASTLE_DOOR_POSITION = new Position(3045, 3371, 0);
    private static final Predicate<SceneObject> CASTLE_DOOR = obj -> obj.getPosition().equals(CASTLE_DOOR_POSITION) && obj.containsAction("Open");

    public static int doQuest() {

        // Process irregular areas (not supported by web walking)
        if (PUB_AREA.contains(Players.getLocal())) {
            Log.info("Getting around pub...");
            Movement.setWalkFlag(SAFE_POSITION);
            Time.sleepUntil(() -> !Players.getLocal().isMoving(), 100, 15000);
            return Config.getLoopReturn();
        }
        if (GATE_AREA.contains(Players.getLocal())) {
            Log.info("Going through gate...");
            processGate();
            return Config.getLoopReturn();
        }

        // Handle Movement
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > Random.nextInt(5, 15)) {
            Movement.toggleRun(true);
            return Config.getLoopReturn();
        }
        if (Players.getLocal().isMoving() && Movement.getDestinationDistance() > Random.nextInt(5, 15)) {
            return Config.getLoopReturn();
        }

        // Quest
        switch (Varps.get(Quest.EASTER_EVENT.getVarp())) {

            case 0: case 5: case 517: case 4613: case 70149:
                Store.setTask("Starting the Quest!");
                processQuestNpc(QuestNpc.EASTER_BUNNY);
                break;

            case 8458762: case 8466954:
                Store.setTask("Grabbin some Silk from Martin");
                processQuestNpc(QuestNpc.MARTIN);
                break;

            case 8475146:
                Pickable tomato = Inventory.contains("Tomato") ? null : Pickables.getNearest("Tomato");
                if (tomato != null) {
                    Store.setTask("Gettin that Tomato");
                    if (tomato.isPositionInteractable())
                        tomato.interact(a->true);
                    else
                        Movement.walkTo(tomato);
                    break;
                }
                Store.setTask("Aggie dyein that Silk");
                processQuestNpc(QuestNpc.AGGIE);
                break;

            case 8483338:
                Store.setTask("Tradin Martin Silk 4 Spices");
                processQuestNpc(QuestNpc.MARTIN);
                break;

            case 8491530: case 8492554: case 8493578:
                Store.setTask("Got milk?.. From the Cook");
                processQuestNpc(QuestNpc.COOK);
                break;

            case 8494602:
                Store.setTask("Askin Merchant about the Dried Cocoa");
                processQuestNpc(QuestNpc.MERCHANT);
                break;

            case 8494666: case 8494730: case 8494794:
                if (!Inventory.contains("Crate of baskets")) {
                    Store.setTask("Gettin a Crate from the Shop Assistant");
                    processQuestNpc(QuestNpc.SHOP_ASSISTANT);
                    break;
                }

                Store.setTask("Gettin that Dried Cocoa from the Merchant");
                processQuestNpc(QuestNpc.MERCHANT);
                break;

            case 8494858:
                Position pos = Players.getLocal().getPosition();
                if (pos.getX() > 3267) {
                    Magic.cast(Spell.Modern.HOME_TELEPORT);
                    Time.sleepUntil(() -> !pos.equals(Players.getLocal().getPosition()), 250, 20000);
                    break;
                }

            case 8494346: case 8490250: case 8424714: case 5541135: case 5541140:
                Store.setTask("Giving everything to the Easter Bunny");
                processQuestNpc(QuestNpc.EASTER_BUNNY);
                break;

            case 5541145: case 5541150: case 5541155: case 22318371: case 22318376:
                Store.setTask("Gettin Party Pete to mix some stuff");
                SceneObject castleDoor = SceneObjects.getNearest(CASTLE_DOOR);
                if (castleDoor != null) {
                    castleDoor.interact(a->true);
                    break;
                }
                processQuestNpc(QuestNpc.PARTY_PETE);
                break;

            case 22318381: case 22318386:
                Store.setTask("Finishing the quest!.. Finally");
                processQuestNpc(QuestNpc.EASTER_BUNNY);
                break;

            default:
                Log.severe("Undefined Varp Value: " + Varps.get(Quest.EASTER_EVENT.getVarp())
                        + " | Report this to the developer.");
        }
        return Config.getLoopReturn();
    }

    private static void processQuestNpc(QuestNpc questNpc) {
        if (!questNpc.getArea().contains(Players.getLocal())) {
            Movement.walkTo(questNpc.getArea().getCenter());
            return;
        }

        if (Dialog.isOpen()) {
            DialogCompleter.processDialog(questNpc.getChatOptions());
            return;
        }

        Npc npc = Npcs.getNearest(questNpc.getName());
        if (npc != null ) {
            if (npc.isPositionInteractable()) {
                npc.interact(a -> true);
            }
            else {
                Movement.walkTo(npc);
            }
        }
    }

    private static void processGate() {
        if (Dialog.isOpen()) {
            DialogCompleter.processDialog();
            return;
        }
        SceneObject gate = SceneObjects.getNearest(obj -> obj.getName().equals("Gate")
                && obj.containsAction(action -> action.startsWith("Pay")));
        if (gate != null) {
            gate.interact(action -> action.startsWith("Pay"));
        }
    }
}
