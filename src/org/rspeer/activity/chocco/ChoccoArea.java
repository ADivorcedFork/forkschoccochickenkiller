package org.rspeer.activity.chocco;

import org.rspeer.runetek.api.movement.position.Area;

public class ChoccoArea {

    public static final Area CAVE = Area.rectangular(2160, 4225,2118,4280);

    public static final Area HOLE_ENTRANCE = Area.rectangular(2971, 3403,2984,3395);

    public static final Area GATE_ENTRANCE = Area.rectangular(2136, 4233,2145,4243);

    public static final Area NEST_ENTRANCE = Area.rectangular(2137, 4244,2145,4250);

    public static final Area CHOCCO_ARENA = Area.rectangular(2132, 4253,2152,4272);

}
