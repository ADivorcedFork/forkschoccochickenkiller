package org.rspeer.activity.chocco;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Equipment;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.ui.Log;
import org.rspeer.store.Config;
import org.rspeer.store.Store;

import java.util.function.Predicate;

public class ChoccoHelper {

    private static Player local;
    private static boolean needsAxe;

    private static final Predicate<Item> AXE = item -> item.getName().endsWith("axe");
    private static final Predicate<Item> PICKAXE = item -> item.getName().endsWith("pickaxe");
    private static final Predicate<Pickable> EASTER_EGG = pickable ->
            pickable.getName().equals("Easter egg") && ChoccoArea.CHOCCO_ARENA.contains(pickable);
    private static final Predicate<SceneObject> SUPPORT = obj -> obj.getName().endsWith("support");

    public static int loop() {

        local = Players.getLocal();

        // Handle Movement
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > Random.nextInt(5, 15)) {
            Movement.toggleRun(true);
            return Config.getLoopReturn();
        }
        if (local.isMoving() && Movement.getDestinationDistance() > Random.nextInt(5, 15)) {
            return Config.getLoopReturn();
        }

        if (!Equipment.contains(PICKAXE)) {
            if (Inventory.contains(PICKAXE))
                Inventory.getFirst(PICKAXE).interact(a->true);
            else
                needsAxe = true;
            return Config.getLoopReturn();
        }

        if (Inventory.isFull() || !hasEquipment()) {
            if (ChoccoArea.CAVE.contains(local)) {
                Store.setTask("Exiting Arena");
                exitArena();
                return Config.getLoopReturn();
            }
            if (Bank.isOpen()) {
                Store.setTask("Banking");
                bank();
                return Config.getLoopReturn();
            }
            Store.setTask("Opening Bank");
            Bank.open(BankLocation.FALADOR_WEST);
            return Config.getLoopReturn();
        }

        if (!ChoccoArea.CAVE.contains(local)) {
            Store.setTask("Entering Cave");
            enterCave();
            return Config.getLoopReturn();
        }
        if (!ChoccoArea.CHOCCO_ARENA.contains(local)) {
            Store.setTask("Entering Arena");
            enterArena();
            return Config.getLoopReturn();
        }

        if (Pickables.getNearest(EASTER_EGG) != null) {
            pickupEasterEggs();
            return Config.getLoopReturn();
        }

        if (SceneObjects.getNearest(SUPPORT) != null) {
            Store.setTask("Attacking support");
            attackSupport();
            return Config.getLoopReturn();
        }

        Store.setTask("Idle");
        return Config.getLoopReturn();
    }

    private static boolean hasEquipment() {
        return Inventory.contains(AXE) && Equipment.contains(PICKAXE);
    }

    private static boolean pickupEasterEggs() {
        Pickable easterEgg = Pickables.getNearest(EASTER_EGG);
        if (easterEgg != null) {
            Store.setTask("Picking up easter eggs");
            return easterEgg.interact(a->true);
        }
        return false;
    }

    private static void bank() {
        Bank.depositAllExcept(AXE);
        Time.sleepUntil(Inventory::isEmpty, 100, 1000);
        Time.sleep(500, 1000);
        if (needsAxe && Bank.contains(AXE)) {
            Bank.withdraw(AXE, 1);
            Time.sleepUntil(() -> Inventory.contains(AXE), 100, 1000);
        }
        else {
            Log.severe("No axe found! Stopping...");
            Config.setStopping(true);
        }
        if (Bank.contains(PICKAXE)) {
            Bank.withdraw(PICKAXE, 1);
            Time.sleepUntil(() -> Inventory.contains(PICKAXE), 100, 1000);
        }
        else {
            Log.severe("No pickaxe found! Stopping...");
            Config.setStopping(true);
        }
        Bank.close();
    }

    private static boolean attackSupport() {
        SceneObject support = SceneObjects.getNearest(SUPPORT);
        if (support != null) {
            return support.interact(a->true);
        }
        return false;
    }


    private static boolean enterCave() {
        SceneObject burrow = SceneObjects.getNearest("Burrow");
        if (burrow != null) {
            return burrow.interact(a->true);
        }
        return Movement.walkTo(ChoccoArea.HOLE_ENTRANCE.getCenter());
    }

    private static void enterArena() {
        if (ChoccoArea.CHOCCO_ARENA.contains(local)) {

        }
        else if (ChoccoArea.NEST_ENTRANCE.contains(local)) {

        }
        else if (ChoccoArea.GATE_ENTRANCE.contains(local)) {

        }
        else if (ChoccoArea.HOLE_ENTRANCE.contains(local)) {

        }
    }

    private static void exitArena() {
        if (ChoccoArea.CHOCCO_ARENA.contains(local)) {

        }
        else if (ChoccoArea.NEST_ENTRANCE.contains(local)) {

        }
        else if (ChoccoArea.GATE_ENTRANCE.contains(local)) {

        }
        else if (ChoccoArea.HOLE_ENTRANCE.contains(local)) {

        }
    }
}
