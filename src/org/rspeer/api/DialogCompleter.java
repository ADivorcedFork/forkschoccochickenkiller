package org.rspeer.api;

import org.rspeer.runetek.api.component.Dialog;

public class DialogCompleter {

    public static void processDialog() {
        processNonOptionsDialog();
    }

    public static void processDialog(int optionIndex) {
        processNonOptionsDialog();
        if (Dialog.isViewingChatOptions()) {
            Dialog.process(optionIndex);
        }
    }

    public static void processDialog(String... options) {
        processNonOptionsDialog();
        if (Dialog.isViewingChatOptions()) {
            Dialog.process(options);
        }
    }

    private static void processNonOptionsDialog() {
        if (!Dialog.isOpen() || Dialog.isProcessing()) {
            return;
        }
        if (Dialog.canContinue()) {
            Dialog.processContinue();
        }
    }
}
