package org.rspeer.main;

import org.rspeer.activity.chocco.ChoccoHelper;
import org.rspeer.activity.quest.EasterEvent;
import org.rspeer.activity.quest.Quest;
import org.rspeer.runetek.api.scene.Projection;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.store.Config;
import org.rspeer.store.Store;
import org.rspeer.ui.Log;

import java.awt.*;

@ScriptMeta(name = "Fork's Chocco Chicken Killer",  desc = "Does Event, Kills Chocco, Gets Eggs", developer = "ADivorcedFork", category = ScriptCategory.MONEY_MAKING)
public class Chocco extends Script implements RenderListener {

    @Override
    public void onStart() {
        super.onStart();
        Log.fine("Starting Fork's Chocco Chicken Killer!");
        Store.startElapsedTime();
    }

    @Override
    public int loop() {
        if (Config.isStopping()) {
            setStopping(true);
            Config.getLoopReturn();
        }
        if (!Quest.EASTER_EVENT.isComplete()) {
            return EasterEvent.doQuest();
        }
        return ChoccoHelper.loop();
    }

    @Override
    public void onStop() {
        Projection.setLowCPUMode(false);
        Log.fine("Thanks for using Fork's Chocco Chicken Killer!");
        Log.fine("Total Runtime: " + Store.getElapsedTimeString());
        super.onStop();
    }

    @Override
    public void notify(RenderEvent e) {
        Graphics g = e.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int y = 35;
        int x = 10;
        g2.setColor(Color.WHITE);
        g2.drawString("Fork's Chocco Chicken Killer!", x, y);
        g2.drawString("Elapsed Time: " + (Store.getElapsedTimeString() != null ? Store.getElapsedTimeString() : ""), x, y += 20);
        g2.drawString("Task: " + Store.getTask(), x, y += 20);
    }
}
